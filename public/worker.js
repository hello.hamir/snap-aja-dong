console.log('Loaded service worker!');

self.addEventListener('push', ev => {
	const data = ev.data.json();
	console.log('Got push', data);

	const isOpen = false;
	if (!isOpen) {
		self.registration.showNotification(data.title, {
			body: data.body,
			icon: 'https://prod.snapaja.id/static/img/logo.a944628.png'
		});
	} else {
		//do add item in notif panel & refresh related pages
	}
});
