'use strict';

module.exports = function(app) {
	const intra = require('./controller/intrabridge');
	const notif = require('./controller/notif');
	const ads = require('./controller/iklan');
	var todoUser = require('./controller/user.js');
	var todoTrans = require('./controller/transaction.js');

	//user api
	app.route('/').get(todoUser.index);
	app.route('/register').post(todoUser.register);
	app.route('/login').post(todoUser.login);
	app.route('/sprofile').post(todoUser.sprofile);
	app.route('/uprofile').post(todoUser.uprofile);
	app.route('/chgpass').post(todoUser.chgPass);
	app.route('/addservice').post(todoUser.addservice);
	app.route('/addservice2').post(todoUser.addservice2);
	app.route('/fgtpass').post(todoUser.fgtpass);

	//transaction api
	app.route('/atrans').post(todoTrans.atrans);
	app.route('/strans').post(todoTrans.strans);
	app.route('/stransAll').post(todoTrans.stransAll);
	app.route('/ctrans').post(todoTrans.ctrans);

	//notif api
	app.route('/subcribe').post(notif.subcribe);
	app.route('/testnotif/:id_user').get(notif.testSendChat);
	app.route('/sendChatNotif').post(notif.sendChatNotif);
	app.route('/getnotif').post(notif.getnotif);
	app.route('/updnotif').post(notif.updnotif);

	//intra api
	app.route('/checkbridge').get(intra.checkIntraBridge);
	app.route('/testbridge/:msg').get(intra.testaja);
	app.route('/checknetwork/:msg').get(intra.getNetworkStat);
	app.route('/sservices').post(intra.getServiceProfile);

	// ads api
	app.route('/getiklans').post(ads.getIklans);
};
