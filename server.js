const dotenv = require('dotenv');
dotenv.config();
const fs = require('fs');
const http = require('http');
const https = require('https');
const routes = require('./routes');
const { bridgeSetup } = require('./controller/intrabridge');

var express = require('express'),
	app = express(),
	port = parseInt(process.env.PORT),
	bodyParser = require('body-parser'),
	cors = require('cors');
const useragent = require('express-useragent');
const cookieParser = require('cookie-parser');

// Certificate;
const privateKey = fs.readFileSync(
	'/etc/letsencrypt/live/snapaja.id/privkey.pem',
	'utf8'
);
const certificate = fs.readFileSync(
	'/etc/letsencrypt/live/snapaja.id/cert.pem',
	'utf8'
);
const ca = fs.readFileSync(
	'/etc/letsencrypt/live/snapaja.id/chain.pem',
	'utf8'
);
const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

// Starting both http & https servers
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

// Intranet Connection
bridgeSetup(httpServer);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
app.use(useragent.express());

routes(app);

app.use('/pubs', express.static('./public'));

httpServer.listen(port, () => {
	console.log('HTTP Server running on port ' + port);
});

httpsServer.listen(port + 1, () => {
	console.log('HTTPS Server running on port ' + (port + 1));
});
