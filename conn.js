const dotenv = require('dotenv');
dotenv.config();
var mysql = require('mysql');

var con = mysql.createConnection({
	host: process.env.HOSTDB,
	user: process.env.USERDB,
	password: process.env.PASSDB,
	database: process.env.DBASE
});

con.connect(function(err) {
	if (err) throw err;
});

module.exports = con;
