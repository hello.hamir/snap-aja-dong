'use strict';

var response = require('../res.js');
var connection = require('../conn.js');
const notif = require('./notif');

exports.atrans = function(req, res) {
	var id_user = req.body.id_user;

	connection.query('SELECT * FROM user where id_user=?', [id_user], function(
		error,
		rows,
		fields
	) {
		if (error) {
			console.log(error);
		} else {
			var service_number = rows[0]['service_number'];

			connection.query(
				"SELECT * FROM transaction where id_user=? and status_ticket='open'",
				[id_user],
				function(error, rows, fields) {
					if (error) {
						console.log(error);
					} else {
						if (rows != '') {
							response.ok('501', res);
						} else {
							if (service_number.charAt(0) == '1') {
								connection.query(
									"SELECT ugamas.phone_number as phone_number, ugamas.internet_number as internet_number, gamas.status as status, gamas.id_ticket_gamas as id_ticket_gamas , gamas.headline_gamas as headline FROM user_gamas as ugamas JOIN ticket_gamas as gamas ON ugamas.id_ticket_gamas=gamas.id_ticket_gamas where internet_number=? and status='open'",
									[service_number],
									function(error, rows, fields) {
										if (error) {
											console.log(error);
										} else {
											if (rows != '') {
												var id_ticket_gamas = rows[0]['id_ticket_gamas'];
												connection.query(
													'INSERT INTO transaction (id_user,id_ticket_gamas,service_number, tdate) values (?,?,?,now())',
													[id_user, id_ticket_gamas, service_number],
													function(error, rows, fields) {
														if (error) {
															console.log(error);
														} else {
															response.ok('204', res);
														}
													}
												);
											} else {
												connection.query(
													'INSERT INTO transaction (id_user,service_number,tdate) values (?,?,now())',
													[id_user, service_number],
													function(error, rows, fields) {
														if (error) {
															console.log(error);
														} else {
															response.ok('204', res);
														}
													}
												);
											}
										}
									}
								);
							} else {
								connection.query(
									"SELECT ugamas.phone_number as phone_number, ugamas.phone_number as phone_number, gamas.status as status, gamas.id_ticket_gamas as id_ticket_gamas , gamas.headline_gamas as headline FROM user_gamas as ugamas JOIN ticket_gamas as gamas ON ugamas.id_ticket_gamas=gamas.id_ticket_gamas where phone_number=? and status='open'",
									[service_number],
									function(error, rows, fields) {
										if (error) {
											console.log(error);
										} else {
											if (rows != '') {
												var id_ticket_gamas = rows[0]['id_ticket_gamas'];
												connection.query(
													'INSERT INTO transaction (id_user,id_ticket_gamas,service_number,tdate) values (?,?,?,now())',
													[id_user, id_ticket_gamas, service_number],
													function(error, rows, fields) {
														if (error) {
															console.log(error);
														} else {
															response.ok('204', res);
														}
													}
												);
											} else {
												connection.query(
													'INSERT INTO transaction (id_user,service_number,tdate) values (?,?,now())',
													[id_user, service_number],
													function(error, rows, fields) {
														if (error) {
															console.log(error);
														} else {
															response.ok('204', res);
														}
													}
												);
											}
										}
									}
								);
							}
						}
					}
				}
			);
		}
	});
};

exports.stransAll = function(req, res) {
	var id_user = req.body.id_user;

	connection.query(
		'select id_transaction, shandling_ticket, tdate from transaction where id_user=?',
		[id_user],
		function(error, rows, fields) {
			if (error) {
				console.log(error);
			} else {
				response.ok(rows, res);
			}
		}
	);
};

exports.strans = function(req, res) {
	var id_user = req.body.id_user;
	var id_transaction = req.body.id_transaction;

	if (id_transaction == null || id_transaction == '') {
		connection.query(
			"select * from transaction where id_user=? and status_ticket='open'",
			[id_user],
			function(error, rows, fields) {
				if (error) {
					console.log(error);
				} else {
					var id_ticket_gamas = rows[0]['id_ticket_gamas'];
					var trans = rows;
					connection.query(
						'select headline_gamas from ticket_gamas where id_ticket_gamas=?',
						[id_ticket_gamas],
						function(error, rows, fields) {
							if (error) {
								console.log(error);
							} else {
								rows[1] = trans;
								response.ok(rows, res);
							}
						}
					);
				}
			}
		);
	} else {
		connection.query(
			'select * from transaction where id_transaction=?',
			[id_transaction],
			function(error, rows, fields) {
				if (error) {
					console.log(error);
				} else {
					var id_ticket_gamas = rows[0]['id_ticket_gamas'];
					var trans = rows;
					connection.query(
						'select headline_gamas from ticket_gamas where id_ticket_gamas=?',
						[id_ticket_gamas],
						function(error, rows, fields) {
							if (error) {
								console.log(error);
							} else {
								rows[1] = trans;
								response.ok(rows, res);
							}
						}
					);
				}
			}
		);
	}
};

exports.ctrans = function(req, res) {
	var id_transaction = req.body.id_transaction;
	var rating = req.body.rating;
	var comment = req.body.comment;

	connection.query(
		"update transaction set status_ticket='close',shandling_ticket='close',test_conn='ok',profile_bw='ok',fup='ok',payment='ok',config='ok',ont='ok',attenuation='ok', rating=?, comment=? where id_transaction=?",
		[rating, comment, id_transaction],
		function(error, rows, fields) {
			if (error) {
				console.log(error);
			} else {
				try {
					if (req.body.id_user)
						notif.sendUpdateTicketNotif(
							{
								id_user: req.body.id_user,
								idtrans: id_transaction,
								status: `close`,
								from: `SNAP-HD`,
								redaksi: `Tiket gangguan anda telah close`
							},
							fn => console.log('SendNotif', fn)
						);
				} catch (error) {
					console.error('Error in ctrans notif', error);
				}
				response.ok('205', res);
			}
		}
	);
};
