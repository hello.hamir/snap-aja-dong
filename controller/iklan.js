const db = require('../conn.js');

module.exports.getIklans = (req, res) => {
	try {
		db.query(
			"select id_ads, type, filter, headline, body, thumbimg, thumbsum, createdat, stats from iklans where (stats is null or stats <> 'expired') and (filter is null or filter not like ?)",
			[`%[${req.body['cust-packet']}]%`],
			(err, rows, info) => {
				res.json({ status: 200, values: rows });
				res.end();
			}
		);
	} catch (err) {
		console.error('error in sendchatnotif', err);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};
