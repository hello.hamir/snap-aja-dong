'use strict';
const Server = require('socket.io');
const password = process.env.IBG_KEY;
const clpass = process.env.IBG_CLI;
const timeout = 90000;
let sver = undefined;
module.exports.bridgeSetup = http => {
	const io = Server(http, {
		path: '/intrabridge'
	});
	io.on('connection', socket => {
		console.log('a user connected');
		socket.on('register', (code, fn) => {
			if (code)
				if ((code = password)) {
					console.log('a server connected');
					if (!sver) sver = socket;
					sver.on('disconnect', () => (sver = undefined));
					fn('server autherize');
				} else fn('server not autherized');
			else fn('hello');
		});
		socket.on('req', (data, fn) => {
			fn({
				status: 'Error : Server not present'
			});
			// console.log(`Server are not present`, typeof sver, data);
		});
		socket.emit('connected', 'hello from server side');
	});
};
const req2Server = (data, cb) => {
	let stats = true;
	let timot = setTimeout(() => {
		stats = false;
		cb({
			status: 401,
			values: 'Error : Timeout Exceeded'
		});
	}, timeout);
	if (sver)
		sver.emit(data.event, data, hsl => {
			if (stats) {
				clearTimeout(timot);
				cb(hsl);
			}
		});
	else {
		cb({
			status: 402,
			values: 'Error : Server not present'
		});
	}
};

module.exports.testaja = (req, res) => {
	try {
		req2Server({ event: 'testaja', query: req.params.msg }, data => {
			res.send(JSON.stringify(data));
			res.end();
		});
	} catch (error) {
		console.log('error testaja', error);
		res.json({ status: 200, values: 501 });
	}
};

module.exports.checkIntraBridge = (req, res) => {
	try {
		res.send(sver ? 'server is connected' : 'server is diconnected');
		res.end();
	} catch (error) {
		console.log('error checkintrabridge', error);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};

module.exports.getNetworkStat = (req, res) => {
	try {
		req2Server({ event: 'getnetstat', nomor: req.params.msg }, data => {
			res.json(JSON.stringify(data));
			res.end();
		});
	} catch (error) {
		console.log('error get netstat', error);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};

module.exports.getServiceProfile = (req, res) => {
	try {
		if (!req.body.service_number) {
			res.json({ status: 200, values: 406 });
			console.log('GetService Profile should include sn');
			return false;
		} else {
			res.json({
				status: 200,
				values: 202,
				dummy: true,
				'cust-quota': '450',
				'cust-usage': '110.91',
				'cust-sisa': '339.09',
				'cust-batas': 'sesuai',
				'cust-packet': 'Indihome FUP',
				'cust-billing': 'warning' //[sudah,belum, warning, isolir]
			});
			res.end();
		}
	} catch (error) {
		console.log('error get service profile', error);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};

module.exports.checknumber = req => {
	return new Promise((resolve, reject) => {
		try {
			req2Server({ event: 'checknumb', body: req.body }, data => {
				resolve(data);
			});
		} catch (error) {
			reject(error);
		}
	});
};
