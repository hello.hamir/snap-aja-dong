'use strict';

var response = require('../res.js');
var connection = require('../conn.js');
var nodemailer = require('nodemailer');
const intra = require('./intrabridge');

exports.user = function(req, res) {
	connection.query('SELECT * FROM user', function(error, rows, fields) {
		if (error) {
			console.log('error getting user', error);
			response.ok('401', res);
		} else {
			response.ok(rows, res);
		}
	});
};

exports.register = function(req, res) {
	var randomstring = require('randomstring');
	var fname = req.body.fname;
	var username = req.body.username;
	var password = req.body.password;
	var cpassword = req.body.cpassword;
	var cust_number = req.body.cust_number;
	var randomstring = randomstring.generate();

	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'snapajadong@gmail.com',
			pass: 'snapsnap123Aja'
		}
	});

	const mailOptions = {
		from: 'snapajadong@gmail.com', // sender address
		to: username, // list of receivers
		subject: 'Email Verifikasi', // Subject line
		html:
			'<p>Hai, Untuk kemudahan kamu menggunakan dengan SnapAja, mohon melakukan verifikasi email kamu melalui link berikut ini:</p> <br></br> <p>http://verif.snapaja.id/index.php?code=' +
			randomstring +
			'</p>' // plain text body
	};

	connection.query('SELECT * FROM user where username=?', [username], function(
		error,
		rows,
		fields
	) {
		if (error) {
			console.log('error checking username in register', error);
			response.ok('401', res);
		} else {
			if (password != cpassword) {
				response.ok('405', res);
			} else if (rows != '') {
				response.ok('406', res);
			} else {
				connection.query(
					'INSERT INTO user (fname,username,password,cust_number,rstring) values (?,?,PASSWORD(?),?,?)',
					[fname, username, password, cust_number, randomstring],
					function(error, rows, fields) {
						if (error) {
							console.log('error registering user', error);
							response.ok('401', res);
						} else {
							response.ok('200', res);
							transporter.sendMail(mailOptions, function(err, info) {});
						}
					}
				);
			}
		}
	});

	// connection.query(
	// 	'INSERT INTO user (fname,username,password,cust_number) values (?,?,PASSWORD(?),?)',
	// 	[fname, username, password, cust_number],
	// 	function(error, rows, fields) {
	// 		if (error) {
	// 			console.log(error);
	// 		} else {
	// 			response.ok('200', res);
	// 		}
	// 	}
	// );
};

exports.login = function(req, res) {
	var username = req.body.username;
	var password = req.body.password;

	connection.query(
		'SELECT id_user,fname, username, package, provider,service_number,cust_number FROM user where username = ? AND password = PASSWORD(?) AND status=1',
		[username, password],
		function(error, rows, fields) {
			if (error) {
				console.log('error in signin', error);
				response.ok('401', res);
			} else {
				if (rows == '') {
					response.ok('404', res);
				} else {
					response.ok(rows, res);
				}
			}
		}
	);
};

exports.sprofile = function(req, res) {
	var id_user = req.body.id_user;

	connection.query(
		'SELECT id_user,fname, username, package, provider,service_number, cust_number FROM user where id_user = ? ',
		[id_user],
		function(error, rows, fields) {
			if (error) {
				console.log('error getting profile', error);
				response.ok('401', res);
			} else {
				response.ok(rows, res);
			}
		}
	);
};

exports.uprofile = function(req, res) {
	var id_user = req.body.id_user;
	var fname = req.body.fname;
	var username = req.body.username;
	var cust_number = req.body.cust_number;

	connection.query(
		'SELECT * FROM user where username=?',
		[username, id_user],
		function(error, rows, fields) {
			if (error) {
				console.log('error get username', error);
				response.ok('401', res);
			} else {
				const id_user_db = rows[0] ? rows[0]['id_user'] : id_user;
				if (rows != '' && id_user_db != id_user) {
					response.ok('406', res);
				} else {
					connection.query(
						'update user set fname=?,username=?,cust_number=? where id_user=?',
						[fname, username, cust_number, id_user],
						function(error, rows, fields) {
							if (error) {
								console.log('error update profile', error);
								response.ok('401', res);
							} else {
								response.ok('202', res);
							}
						}
					);
				}
			}
		}
	);
	// if (password != null) {
	// 	connection.query(
	// 		'update user set fname=?,username=?,password=password(?),cust_number=? where id_user=? ',
	// 		[fname, username, password, cust_number, id_user],
	// 		function(error, rows, fields) {
	// 			if (error) {
	// 				console.log(error);
	// 			} else {
	// 				response.ok('202', res);
	// 			}
	// 		}
	// 	);
	// } else {
	// 	connection.query(
	// 		'update user set fname=?,username=?,cust_number=? where id_user',
	// 		[fname, username, cust_number, id_user],
	// 		function(error, rows, fields) {
	// 			if (error) {
	// 				console.log(error);
	// 			} else {
	// 				response.ok('202', res);
	// 			}
	// 		}
	// 	);
	// }
};

exports.chgPass = function(req, res) {
	try {
		connection.query(
			'update user set password=password(?) where id_user=?',
			[req.body.password, req.body.id_user],
			(error, rows, fields) => {
				if (error) {
					console.log('error update pass', error);
					response.ok('401', res);
				} else {
					response.ok('202', res);
				}
			}
		);
	} catch (err) {
		console.log('error change pass', error);
		response.ok('501', res);
	}
};

exports.addservice = function(req, res) {
	var id_user = req.body.id_user;
	var service_number = req.body.service_number;
	connection.query(
		'update user set service_number=? where id_user=? ',
		[service_number, id_user],
		function(error, rows, fields) {
			if (error) {
				console.log('error update service number', error);
				response.ok('401', res);
			} else {
				response.ok('203', res);
			}
		}
	);
};

exports.addservice2 = function(req, res) {
	var id_user = req.body.id_user;
	var service_number = req.body.service_number;

	//check service number
	try {
		intra
			.checknumber({
				body: {
					service_number: req.body.service_number,
					name: req.body.name,
					attempt: req.body.attempt
				}
			})
			.then(dt => {
				if (dt.status == 'ok') {
					connection.query(
						'update user set service_number=? where id_user=? ',
						[service_number, id_user],
						function(error, rows, fields) {
							if (error) {
								console.log('error update service number', error);
								response.ok('401', res);
							} else {
								response.ok('203', res);
							}
						}
					);
				} else {
					res.json(dt);
					res.end();
				}
			})
			.catch(err => {
				console.log('error in check number', err);
				response.ok('401', res);
			});
	} catch (err) {
		console.log('error in add service', err);
	}
};

exports.fgtpass = function(req, res) {
	var randomstring = require('randomstring');
	var username = req.body.username;
	var randomstring = randomstring.generate(7);

	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'snapajadong@gmail.com',
			pass: 'snapsnap123Aja'
		}
	});

	const mailOptions = {
		from: 'snapajadong@gmail.com', // sender address
		to: username, // list of receivers
		subject: 'Password Baru SnapAja', // Subject line
		html:
			'<p>Terima kasih telah melakukan reset password di akun website SnapAja.<br></br> Berikut ini adalah password yang telah di rubah di sistem kami :</p>' +
			randomstring // plain text body
	};

	transporter.sendMail(mailOptions, function(err, info) {
		if (err) response.ok('406', res);
		else
			connection.query(
				'update user set password=password(?) where username=? ',
				[randomstring, username],
				function(error, rows, fields) {
					if (error) {
						console.log('error update service number', error);
						response.ok('401', res);
					} else {
						response.ok('206', res);
					}
				}
			);
	});
};

exports.index = function(req, res) {
	response.ok('Welcome to the dark side !!!!', res);
};
