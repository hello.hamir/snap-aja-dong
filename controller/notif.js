const webpush = require('web-push');
const publicVapidKey = process.env.PUBVKEY;
const privateVapidKey = process.env.PRIVKEYV;
const db = require('../conn.js');

webpush.setVapidDetails(
	'mailto:snapajadong@gmail.com',
	publicVapidKey,
	privateVapidKey
);

const getDevice = ua => {
	// console.log(ua);
	let hasil = 'unknown';
	if (ua.isDesktop) {
		hasil = 'Desktop ' + ua.browser + ' ' + ua.os;
	} else {
		const rwdevid = ua.source.match(/\((.*?)\)/g);
		const ardevid = rwdevid !== false ? rwdevid[0].split(';') : [0];
		hasil = ua.platform + ' ' + ua.browser;
		if (ardevid[2])
			hasil += ' ' + ardevid[2].substr(0, ardevid[2].length - 1).trim();
	}
	return hasil;
};

const subcribe = (req, res) => {
	try {
		if (typeof req.body.subs !== 'object') throw new Error('SnapApp Only');
		if (req.body.cokies) {
			const cookies = req.body.cokies
				.split(/[;] */)
				.reduce(function(result, pairStr) {
					var arr = pairStr.split('=');
					if (arr.length === 2) {
						result[arr[0]] = arr[1];
					}
					return result;
				}, {});
			if (cookies['devicecd'] == `dcode${req.body.id_user}`) {
				console.error('Already Subscribe');
				res.json({ status: 200, values: 404, msg: `Already Subscribe` });
				res.end();
				return false;
			}
		}

		db.query(
			`insert into user_devices (id_user,subsdevice,subs,created) values (?,?,?,now())`,
			[req.body.id_user, getDevice(req.useragent), JSON.stringify(req.body.subs)],
			(err, hsl, info) => {
				if (err) {
					console.error(err);
					res.json({ status: 200, values: 501 });
					return false;
				}
				res.status(201).json({
					status: 200,
					values: `udevice${hsl.insertId}`,
					cqs: {
						ddevice: `udevice${hsl.insertId}`,
						devicecd: `dcode${req.body.id_user}`
					}
				});
				res.end();
			}
		);
	} catch (err) {
		console.error('error in subscribe notif', err);
		res.json({ status: 200, values: 401 });
		res.end();
	}
};

const pushNotif = (subs, data) => {
	try {
		if (!subs) throw new Error('destination api not provided');
		subs = JSON.parse(subs);
		//verify payload
		const payload = JSON.stringify({
			type: data.type, //[netcheck,gamas,ticket,chat,ads]
			idtrans: data.idtrans,
			status: data.status, //[new, ogp, close]
			title: data.title,
			body: data.body
		});
		webpush
			.sendNotification(subs, payload)
			.then(res => {
				return res;
			})
			.catch(error => {
				console.error('error webpush :', error.stack);
			});
	} catch (err) {
		console.error('error in sending notif', err);
	}
};

const regisNotif = (id_user, payload, cb) => {
	try {
		db.query(
			'select subs from user_devices where id_user = ?',
			[id_user],
			(err, rows, info) => {
				if (err || rows.length == 0) {
					cb({ error: err || 'not subscribe yet' });
				} else {
					rows.forEach(async row => {
						pushNotif(row['subs'], payload);
					});
					db.query(
						'insert into notifications (id_user, type, id_trans, `status`, title, body, createdat) values (?,?,?,?,?,?,now())',
						[
							id_user,
							payload.type,
							payload.idtrans,
							payload.status,
							payload.title,
							payload.body
						],
						(err, hsl, info) => {
							if (err) {
								console.error(err);
								cb({ error: err });
								return false;
							}
							cb({ devices: rows.length });
						}
					);
				}
			}
		);
	} catch (err) {
		console.log(err);
		cb({ error: err });
	}
};

module.exports = { subcribe, regisNotif };

module.exports.sendChatNotif = (req, res) => {
	try {
		const payload = {
			type: 'chat', //[netcheck,gamas,ticket,chat]
			idtrans: req.body.id_chat,
			status: 'new', //[new, ogp, close]
			title: req.body.from || 'SNAP-HD',
			body: req.body.msg
		};
		regisNotif(req.body.id_user, payload, hsl => {
			if (hsl.error) {
				console.error('error regis notif', hsl.error);
				res.json({ status: 200, values: 501 });
				return false;
			}
			res.json({ status: 200, values: 200, msg: `success to ${hsl.devices}` });
			res.end();
		});
	} catch (err) {
		console.error('error in sendchatnotif', err);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};

module.exports.sendUpdateTicketNotif = (data, fn) => {
	try {
		const payload = {
			type: 'ticket', //[netcheck,gamas,ticket,chat]
			idtrans: data.idtrans,
			status: data.status, //[new, ogp, close]
			title: data.from,
			body: data.redaksi
		};
		regisNotif(data.id_user, payload, hsl => {
			if (hsl.error) {
				console.error('error regis notif', hsl.error);
				fn({ status: 200, values: 501 });
				return false;
			}
			fn({ status: 200, values: 200, msg: `success to ${hsl.devices}` });
		});
	} catch (err) {
		console.error('error in sendchatnotif', err);
		fn({ status: 200, values: 501 });
	}
};

module.exports.testSendChat = (req, res) => {
	try {
		const payload = {
			type: 'chat',
			idtrans: req.params.id_user,
			status: 'new',
			title: 'SNAP-TEST',
			body: `Please Ignore\nNotification from : ${getDevice(req.useragent)}`
		};
		regisNotif(req.params.id_user, payload, hsl => {
			if (hsl.error) {
				console.error(hsl.error);
				res.json({ status: 200, values: 501 });
				return false;
			}
			res.json({ status: 200, values: 200, msg: `success to ${hsl.devices}` });
			res.end();
		});
	} catch (err) {
		console.log('error test notif', err);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};

module.exports.getnotif = (req, res) => {
	try {
		db.query(
			"select id_notif, type, id_trans, `status`, title, body, stats, createdat from notifications where (stats is null or stats<>'delete') and id_user=?",
			[req.body.id_user],
			(err, rows, info) => {
				if (err) {
					console.error('error in getnotif', err);
					res.json({ status: 200, values: 501 });
					return false;
				}
				res.json({ status: 200, values: rows });
				res.end();
			}
		);
	} catch (err) {
		console.error('error in sendchatnotif', err);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};

module.exports.updnotif = (req, res) => {
	try {
		if (req.body.stats == 'reads') {
			db.query(
				'update notifications set stats=? where id_notif in (?)',
				[req.body.stats, req.body.id_notifs.split(',')],
				(err, rows, info) => {
					if (err) {
						console.error(err);
						res.json({ status: 200, values: 501 });
						return false;
					}
					res.json({ status: 200, values: 202, success: rows.changedRows });
					res.end();
				}
			);
		} else if (req.body.stats == 'deletes') {
			db.query(
				'delete from notifications where id_notif in (?)',
				[req.body.id_notifs.split(',')],
				(err, rows, info) => {
					if (err) {
						console.error(err);
						res.json({ status: 200, values: 501 });
						return false;
					}
					res.json({ status: 200, values: 202, success: rows.affectedRows });
					res.end();
				}
			);
		} else {
			res.json({ status: 200, values: 402 });
			res.end();
		}
	} catch (error) {
		console.error('error in sendchatnotif', err);
		res.json({ status: 200, values: 501 });
		res.end();
	}
};
